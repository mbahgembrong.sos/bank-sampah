<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('nasabah')->group(function () {
    Route::get('/login', [App\Http\Controllers\Auth\AuthPelangganController::class, 'login'])->name('login.nasabah');
    Route::post('/login', [App\Http\Controllers\Auth\AuthPelangganController::class, 'postLogin'])->name('login.post.nasabah');
    Route::get('/register', [App\Http\Controllers\Auth\AuthPelangganController::class, 'register'])->name('register.nasabah');
    Route::post('/register', [App\Http\Controllers\Auth\AuthPelangganController::class, 'postRegister'])->name('register.post.nasabah');
    Route::post('/logout', [App\Http\Controllers\Auth\AuthPelangganController::class, 'logout'])->name('logout.nasabah');
});

Route::prefix('petugas')->group(function () {
    Route::get('/login', [App\Http\Controllers\Auth\AuthPetugasController::class, 'login'])->name('login.petugas');
    Route::post('/login', [App\Http\Controllers\Auth\AuthPetugasController::class, 'postLogin'])->name('login.post.petugas');
    Route::post('/logout', [App\Http\Controllers\Auth\AuthPetugasController::class, 'logout'])->name('logout.petugas');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
Route::resource('nasabahs', App\Http\Controllers\NasabahController::class);
Route::resource('petugas', App\Http\Controllers\PetugasController::class);
Route::resource('jenisSampahs', App\Http\Controllers\JenisSampahController::class);
Route::resource('bukuTabungans', App\Http\Controllers\BukuTabunganController::class);
Route::resource('setorans', App\Http\Controllers\SetoranController::class);
Route::resource('bayarJasas', App\Http\Controllers\BayarJasaController::class);
Route::resource('sampahs', App\Http\Controllers\SampahController::class);
Route::resource('sampahMasuks', App\Http\Controllers\SampahMasukController::class);
Route::prefix('setoran')->group(function () {
    Route::get('/', [App\Http\Controllers\SetoranController::class, 'index'])->name('setoran.index');
    Route::get('/create', [App\Http\Controllers\SetoranController::class, 'create'])->name('setoran.create');
    Route::post('/store', [App\Http\Controllers\SetoranController::class, 'store'])->name('setoran.store');
});
Route::prefix('bayarjasa')->group(function () {
    Route::get('/', [App\Http\Controllers\BayarJasaController::class, 'index'])->name('bayarjasa.index');
    Route::get('/create', [App\Http\Controllers\BayarJasaController::class, 'create'])->name('bayarjasa.create');
    Route::post('/store', [App\Http\Controllers\BayarJasaController::class, 'store'])->name('bayarjasa.store');
});
