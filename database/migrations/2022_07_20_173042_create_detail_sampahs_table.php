<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailSampahsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_sampahs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('sampah_id');
            $table->integer('harga');
            $table->timestamps();
        });
        Schema::table('detail_sampahs', function (Blueprint  $table) {
            $table->foreign('sampah_id')->references('id')->on('sampahs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_sampahs');
    }
}
