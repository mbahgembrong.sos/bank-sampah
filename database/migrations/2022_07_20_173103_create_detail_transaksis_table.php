<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTransaksisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksis', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('tabungan_id');
            $table->uuid('sampah_id');
            $table->integer('jumlah');
            $table->integer('sub_total_transaksi');
            $table->timestamps();
        });
        Schema::table('detail_transaksis', function (Blueprint  $table) {
            $table->foreign('tabungan_id')->references('id')->on('buku_tabungans')->onDelete('cascade');
            $table->foreign('sampah_id')->references('id')->on('sampahs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_transaksis');
    }
}
