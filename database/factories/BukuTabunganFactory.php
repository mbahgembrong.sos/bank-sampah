<?php

namespace Database\Factories;

use App\Models\BukuTabungan;
use Illuminate\Database\Eloquent\Factories\Factory;

class BukuTabunganFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BukuTabungan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nasabah_id' => $this->faker->word,
        'petugas_id' => $this->faker->word,
        'jenis_transaksi' => $this->faker->word,
        'jumlah' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
