<!-- Jumlah Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_transaksi', 'Jenis Transaksi:') !!}
    {!! Form::select('jenis_transaksi', $jenisTransaksis, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    {!! Form::number('jumlah', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bukuTabungans.index') }}" class="btn btn-light">Cancel</a>
</div>
