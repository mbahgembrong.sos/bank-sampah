<!-- Nasabah Id Field -->
<div class="form-group">
    {!! Form::label('nasabah_id', 'Nasabah Id:') !!}
    <p>{{ $bukuTabungan->nasabah_id }}</p>
</div>

<!-- Petugas Id Field -->
<div class="form-group">
    {!! Form::label('petugas_id', 'Petugas Id:') !!}
    <p>{{ $bukuTabungan->petugas_id }}</p>
</div>

<!-- Jenis Transaksi Field -->
<div class="form-group">
    {!! Form::label('jenis_transaksi', 'Jenis Transaksi:') !!}
    <p>{{ $bukuTabungan->jenis_transaksi }}</p>
</div>

<!-- Jumlah Field -->
<div class="form-group">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    <p>{{ $bukuTabungan->jumlah }}</p>
</div>

