<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>

<li class="side-menus {{ Request::is('nasabahs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('nasabahs.index') }}"><i class="fas fa-building"></i><span>Nasabahs</span></a>
</li>

<li class="side-menus {{ Request::is('petugas*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('petugas.index') }}"><i class="fas fa-building"></i><span>Petugas</span></a>
</li>

<li class="side-menus {{ Request::is('jenisSampahs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('jenisSampahs.index') }}"><i class="fas fa-building"></i><span>Jenis
            Sampahs</span></a>
</li>


<li class="side-menus {{ Request::is('bukuTabungans*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('bukuTabungans.index') }}"><i class="fas fa-building"></i><span>Buku
            Tabungans</span></a>
</li>
<li class="side-menus {{ Request::is('bayarJasas*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('bayarJasas.index') }}"><i class="fas fa-building"></i><span>Bayar
            Jasas</span></a>
</li>
<li class="side-menus {{ Request::is('setorans*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('setorans.index') }}"><i class="fas fa-building"></i><span>Setoran</span></a>
</li>


<li class="side-menus {{ Request::is('sampahs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('sampahs.index') }}"><i class="fas fa-building"></i><span>Sampahs</span></a>
</li>
<li class="side-menus {{ Request::is('sampahMasuks*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('sampahMasuks.index') }}"><i class="fas fa-building"></i><span>Sampah
            Masuk</span></a>
</li>
