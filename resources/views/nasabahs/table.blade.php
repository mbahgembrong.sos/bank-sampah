<div class="table-responsive">
    <table class="table" id="nasabahs-table">
        <thead>
            <tr>
                <th>Nama</th>
        <th>Email</th>
        <th>Password</th>
        <th>Saldo</th>
        <th>Alamat</th>
        <th>No Hp</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($nasabahs as $nasabah)
            <tr>
                       <td>{{ $nasabah->nama }}</td>
            <td>{{ $nasabah->email }}</td>
            <td>{{ $nasabah->password }}</td>
            <td>{{ $nasabah->saldo }}</td>
            <td>{{ $nasabah->alamat }}</td>
            <td>{{ $nasabah->no_hp }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['nasabahs.destroy', $nasabah->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('nasabahs.show', [$nasabah->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('nasabahs.edit', [$nasabah->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
        @endforeach
        </tbody>
    </table>
</div>
