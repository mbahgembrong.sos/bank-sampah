@extends('layouts.app')
@section('title')
    Nasabahs 
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Nasabahs</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('nasabahs.create')}}" class="btn btn-primary form-btn">Nasabah <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('nasabahs.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection

