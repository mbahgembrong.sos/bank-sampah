@extends('layouts.app')
@section('title')
    Sampahs 
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Sampahs</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('sampahs.create')}}" class="btn btn-primary form-btn">Sampah <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('sampahs.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection

