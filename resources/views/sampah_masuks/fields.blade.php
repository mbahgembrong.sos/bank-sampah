<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control','maxlength' => 255]) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('jenis_sampah_id', 'Jenis Sampah:') !!}
    {!! Form::select('jenis_sampah_id', $jenisSampahs, null, ['class' => 'form-control']) !!}
</div>

<!-- Satuan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('satuan', 'Satuan:') !!}
    {!! Form::select('satuan', $satuans, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sampahs.index') }}" class="btn btn-light">Cancel</a>
</div>
