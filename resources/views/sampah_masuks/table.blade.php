<div class="table-responsive">
    <table class="table" id="sampahs-table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Jenis Sampah</th>
                <th>Satuan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sampahs as $sampah)
                <tr>
                    <td>{{ $sampah->nama }}</td>
                    <td>{{ $sampah->jenis_sampah->nama }}</td>
                    <td>{{ $sampah->satuan }}</td>
                    <td class=" text-center">
                        {!! Form::open(['route' => ['sampahs.destroy', $sampah->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{!! route('sampahs.show', [$sampah->id]) !!}" class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                            <a href="{!! route('sampahs.edit', [$sampah->id]) !!}" class='btn btn-warning action-btn edit-btn'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger action-btn delete-btn',
                                'onclick' => 'return confirm("Are you sure want to delete this record ?")',
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
