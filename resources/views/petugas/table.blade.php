<div class="table-responsive">
    <table class="table" id="petugas-table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>Password</th>
                <th>Alamat</th>
                <th>No Hp</th>
                <th>Role</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($petugas as $petugas)
                <tr>
                    <td>{{ $petugas->nama }}</td>
                    <td>{{ $petugas->email }}</td>
                    <td>{{ $petugas->password }}</td>
                    <td>{{ $petugas->alamat }}</td>
                    <td>{{ $petugas->no_hp }}</td>
                    <td>{{ $petugas->role }}</td>
                    <td class=" text-center">
                        {!! Form::open(['route' => ['petugas.destroy', $petugas->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{!! route('petugas.show', [$petugas->id]) !!}" class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                            <a href="{!! route('petugas.edit', [$petugas->id]) !!}" class='btn btn-warning action-btn edit-btn'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger action-btn delete-btn',
                                'onclick' => 'return confirm("Are you sure want to delete this record ?")',
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
