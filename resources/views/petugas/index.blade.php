@extends('layouts.app')
@section('title')
    Petugas 
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Petugas</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('petugas.create')}}" class="btn btn-primary form-btn">Petugas <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('petugas.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection

