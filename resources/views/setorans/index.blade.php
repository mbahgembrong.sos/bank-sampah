@extends('layouts.app')
@section('title')
    Buku Tabungans 
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Buku Tabungans</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('bukuTabungans.create')}}" class="btn btn-primary form-btn">Buku Tabungan <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('buku_tabungans.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection

