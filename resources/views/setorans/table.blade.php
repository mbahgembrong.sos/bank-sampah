<div class="table-responsive">
    <table class="table" id="bukuTabungans-table">
        <thead>
            <tr>
                <th>Nasabah</th>
                <th>Petugas</th>
                <th>Jenis Transaksi</th>
                <th>Jumlah</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($bukuTabungans as $bukuTabungan)
                <tr>
                    <td>{{ $bukuTabungan->nasabah->nama }}</td>
                    <td>{{ $bukuTabungan->petugas->nama }}</td>
                    <td>{{ $bukuTabungan->jenis_transaksi }}</td>
                    <td>{{ $bukuTabungan->jumlah }}</td>
                    <td class=" text-center">
                        {!! Form::open(['route' => ['bukuTabungans.destroy', $bukuTabungan->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{!! route('bukuTabungans.show', [$bukuTabungan->id]) !!}" class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                            <a href="{!! route('bukuTabungans.edit', [$bukuTabungan->id]) !!}" class='btn btn-warning action-btn edit-btn'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger action-btn delete-btn',
                                'onclick' => 'return confirm("Are you sure want to delete this record ?")',
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
