@extends('layouts.app')
@section('title')
    Jenis Sampahs 
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Jenis Sampahs</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('jenisSampahs.create')}}" class="btn btn-primary form-btn">Jenis Sampah <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('jenis_sampahs.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection

