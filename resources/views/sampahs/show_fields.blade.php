<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $sampah->nama }}</p>
</div>

<!-- Jenis Sampah Id Field -->
<div class="form-group">
    {!! Form::label('jenis_sampah_id', 'Jenis Sampah Id:') !!}
    <p>{{ $sampah->jenis_sampah_id }}</p>
</div>

<!-- Satuan Field -->
<div class="form-group">
    {!! Form::label('satuan', 'Satuan:') !!}
    <p>{{ $sampah->satuan }}</p>
</div>

