<?php

return array (
  'singular' => 'Nasabah',
  'plural' => 'Nasabahs',
  'fields' => 
  array (
    'id' => 'Id',
    'nama' => 'Nama',
    'email' => 'Email',
    'password' => 'Password',
    'saldo' => 'Saldo',
    'alamat' => 'Alamat',
    'no_hp' => 'No Hp',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
