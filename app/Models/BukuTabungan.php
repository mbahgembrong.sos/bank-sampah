<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;

/**
 * Class BukuTabungan
 * @package App\Models
 * @version July 20, 2022, 5:29 pm UTC
 *
 * @property \App\Models\Nasabah $nasabah
 * @property \App\Models\Petugas $petugas
 * @property uuid $nasabah_id
 * @property uuid $petugas_id
 * @property string $jenis_transaksi
 * @property integer $jumlah
 */
class BukuTabungan extends Model
{

    use HasFactory;

    public $table = 'buku_tabungans';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public $fillable = [
        'nasabah_id',
        'petugas_id',
        'jenis_transaksi',
        'jumlah'
    ];
    public $jenis_transaksis = ['setoran', 'tarik', 'bayar jasa'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'jenis_transaksi' => 'string',
        'jumlah' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nasabah_id' => 'required|string',
        'petugas_id' => 'required|string',
        'jenis_transaksi' => 'required',
        'jumlah' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function nasabah()
    {
        return $this->belongsTo(\App\Models\Nasabah::class, 'nasabah_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function petugas()
    {
        return $this->belongsTo(\App\Models\Petugas::class, 'petugas_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
