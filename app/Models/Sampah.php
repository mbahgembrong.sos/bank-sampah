<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;
/**
 * Class Sampah
 * @package App\Models
 * @version July 20, 2022, 5:56 pm UTC
 *
 * @property string $nama
 * @property uuid $jenis_sampah_id
 * @property string $satuan
 */
class Sampah extends Model
{

    use HasFactory;

    public $table = 'sampahs';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $fillable = [
        'nama',
        'jenis_sampah_id',
        'satuan'
    ];
    public $satuans= ['kg', 'biji', 'karung'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama' => 'string',
        'satuan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required|max:255|string',
        'jenis_sampah_id' => 'required',
        'satuan' => 'required'
    ];
    function jenis_sampah()
    {
        return $this->belongsTo('App\Models\JenisSampah', 'jenis_sampah_id');
    }
    function detail_transaksi()
    {
        return $this->hasMany('App\Models\DetailTransaksi', 'sampah_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

}
