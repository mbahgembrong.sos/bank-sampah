<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Ramsey\Uuid\Uuid as Generator;
/**
 * Class Nasabah
 * @package App\Models
 * @version July 20, 2022, 5:28 pm UTC
 *
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property integer $saldo
 * @property string $alamat
 * @property string $no_hp
 */
class Nasabah extends Authenticatable
{

    use HasFactory;
    use Notifiable;
    public $table = 'nasabahs';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $fillable = [
        'nama',
        'email',
        'password',
        'saldo',
        'alamat',
        'no_hp'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama' => 'string',
        'email' => 'string',
        'password' => 'string',
        'saldo' => 'integer',
        'alamat' => 'string',
        'no_hp' => 'string'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required|string',
        'email' => 'required|email|unique:nasabahs,email',
        'password' => 'required|string|min:6',
        'alamat' => 'required|string',
        'no_hp' => 'required|numeric'
    ];
    public function tabungan()
    {
        return $this->hasMany('App\Models\BukuTabungan', 'nasabah_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }


}
