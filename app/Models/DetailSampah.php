<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailSampah
 * @package App\Models
 * @version July 20, 2022, 5:30 pm UTC
 *
 * @property \App\Models\Sampah $sampah
 */
class DetailSampah extends Model
{

    use HasFactory;

    public $table = 'detail_sampahs';

    protected $primaryKey = 'id';
    public $incrementing = false;

    public $fillable = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'harga' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'sampah_id' => 'null'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sampah()
    {
        return $this->belongsTo(\App\Models\Sampah::class, 'sampah_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
