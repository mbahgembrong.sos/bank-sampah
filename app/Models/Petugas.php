<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Ramsey\Uuid\Uuid as Generator;
/**
 * Class Petugas
 * @package App\Models
 * @version July 20, 2022, 5:28 pm UTC
 *
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string $alamat
 * @property string $no_hp
 * @property string $role
 */
class Petugas extends Authenticatable
{
    use Notifiable;
    use HasFactory;

    public $table = 'petugas';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $fillable = [
        'nama',
        'email',
        'password',
        'alamat',
        'no_hp',
        'role'
    ];
    public $roles = ['Petugas', 'Admin'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama' => 'string',
        'email' => 'string',
        'password' => 'string',
        'alamat' => 'string',
        'no_hp' => 'string',
        'role' => 'string'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required|string',
        'email' => 'required|email|unique:petugas,email',
        'password' => 'required|string|min:6',
        'alamat' => 'required|string',
        'no_hp' => 'required|numeric',
        'role' => 'required'
    ];
    public function tabungan()
    {
        return $this->hasMany('App\Models\BukuTabungan', 'nasabah_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }


}
