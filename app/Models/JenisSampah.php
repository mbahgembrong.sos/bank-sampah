<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;
/**
 * Class JenisSampah
 * @package App\Models
 * @version July 20, 2022, 5:29 pm UTC
 *
 * @property string $nama
 */
class JenisSampah extends Model
{

    use HasFactory;

    public $table = 'jenis_sampahs';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $fillable = [
        'nama'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required|string'
    ];
    function sampah()
    {
        return $this->hasMany('App\Models\Sampah', 'jenis_sampah_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

}
