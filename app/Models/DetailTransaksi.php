<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;
/**
 * Class DetailTransaksi
 * @package App\Models
 * @version July 20, 2022, 5:31 pm UTC
 *
 */
class DetailTransaksi extends Model
{

    use HasFactory;

    public $table = 'detail_transaksis';

    protected $primaryKey = 'id';
    public $incrementing = false;

    public $fillable = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'jumlah' => 'integer',
        'sub_total_transaksi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tabungan_id' => 'null',
        'sampah_id' => 'null',
        'jumlah' => 'null',
        'sub_total_transaksi' => 'null'
    ];
    function sampah()
    {
        return $this->belongsTo('App\Models\Sampah', 'sampah_id');
    }
    function tabungan()
    {
        return $this->belongsTo('App\Models\BukuTabungan', 'tabungan_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }


}
