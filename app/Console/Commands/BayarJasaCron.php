<?php

namespace App\Console\Commands;

use App\Models\BukuTabungan;
use App\Models\Nasabah;
use App\Models\Petugas;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BayarJasaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pelanggan:bayar-jasa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Schedule Bayar Jasa Pelanggan In every month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $nasabahs = Nasabah::all();
        foreach ($nasabahs as $nasabah) {
            $bukuTabungan = BukuTabungan::where('nasabah_id', $nasabah->id)->whereMonth('created_at', '=', date('m'))->update(['jenis_transaksi' => 'bayar jasa'])->first();
            if ($bukuTabungan != null) {
                $tabungan = new BukuTabungan();
                $tabungan->petugas_id = Petugas::all()->random()->id;
                $tabungan->nasabah_id = $nasabah->id;
                $tabungan->jenis_transaksi = 'bayar jasa';
                $tabungan->jumlah = env('BAYAR_JASA', 5000);
                $tabungan->keterangan = 'generate system';
                $tabungan->save();
                Log::info('success generate bayar jasa for nasabah ' . $nasabah->nama);
            } else {
                Log::info($nasabah->nama . ' not generate bayar jasa');
            }
        }
        return 0;
    }
}
