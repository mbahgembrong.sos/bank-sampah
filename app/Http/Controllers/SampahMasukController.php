<?php

namespace App\Http\Controllers;

use App\Models\BukuTabungan;
use App\Models\Sampah;
use Illuminate\Http\Request;

class SampahMasukController extends Controller
{
    public function __construct()
    {
        $this->sampah = new Sampah();
    }
    /**
     * Display a listing of the Sampah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Sampah $sampahs */
        $sampahs = Sampah::all();
        $bukuTabungans  = BukuTabungan::all();

        return view('sampah_masuks.index',compact('sampahs','bukuTabungans'));

    }
}
