<?php

namespace App\Http\Controllers;

use App\Models\BukuTabungan;
use CreateBukuTabungansTable;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class BayarJasaController extends Controller
{
    public function __construct()
    {
        $this->bukuTabungan = new BukuTabungan();
    }
    /**
     * Display a listing of the BukuTabungan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var BukuTabungan $bukuTabungans */
        $bukuTabungans = BukuTabungan::all();

        return view('bayar_jasas.index')
            ->with('bukuTabungans', $bukuTabungans);
    }

    /**
     * Show the form for creating a new BukuTabungan.
     *
     * @return Response
     */
    public function create()
    {
        $jenisTransaksis = $this->bukuTabungan->jenis_transaksis;
        return view('bayar_jasas.create', compact('jenisTransaksis'));
    }

    /**
     * Store a newly created BukuTabungan in storage.
     *
     * @param CreateBukuTabunganRequest $request
     *
     * @return Response
     */
    public function store(CreateBukuTabungansTable $request)
    {
        $input = $request->all();
        $bayarJasaPaid
            = BukuTabungan::where('nasabah_id', $request->nasabah_id)->whereMonth('created_at', '=', date('m'))->update(['jenis_transaksi' => 'bayar jasa'])->first();
        if ($bayarJasaPaid != null) {

            /** @var BukuTabungan $bukuTabungan */
            $bukuTabungan = BukuTabungan::create($input);
            Flash::success('Bayar Jasa Nasabah ' . $bukuTabungan->nasabah->nama . ' Berhasil ditambahkan pada bulan ' . date('F'));

            return redirect(route('bayarJasa.index'));
        } else {
            Flash::error('Bayar Jasa ' . $request->nasabah_id . ' sudah terbayar pada bulan ' . date('F'));
            return redirect(route('bayarJasa.index'));
        }
    }
}
