<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBukuTabunganRequest;
use App\Http\Requests\UpdateBukuTabunganRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\BukuTabungan;
use Illuminate\Http\Request;
use Flash;
use Response;

class BukuTabunganController extends AppBaseController
{
    public function __construct() {
        $this->bukuTabungan=new BukuTabungan();
    }
    /**
     * Display a listing of the BukuTabungan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var BukuTabungan $bukuTabungans */
        $bukuTabungans = BukuTabungan::all();

        return view('buku_tabungans.index')
            ->with('bukuTabungans', $bukuTabungans);
    }

    /**
     * Show the form for creating a new BukuTabungan.
     *
     * @return Response
     */
    public function create()
    {
        $jenisTransaksis = $this->bukuTabungan->jenis_transaksis;
        return view('buku_tabungans.create',compact('jenisTransaksis'));
    }

    /**
     * Store a newly created BukuTabungan in storage.
     *
     * @param CreateBukuTabunganRequest $request
     *
     * @return Response
     */
    public function store(CreateBukuTabunganRequest $request)
    {
        $input = $request->all();

        /** @var BukuTabungan $bukuTabungan */
        $bukuTabungan = BukuTabungan::create($input);

        Flash::success('Buku Tabungan saved successfully.');

        return redirect(route('bukuTabungans.index'));
    }

    /**
     * Display the specified BukuTabungan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BukuTabungan $bukuTabungan */
        $bukuTabungan = BukuTabungan::find($id);

        if (empty($bukuTabungan)) {
            Flash::error('Buku Tabungan not found');

            return redirect(route('bukuTabungans.index'));
        }

        return view('buku_tabungans.show')->with('bukuTabungan', $bukuTabungan);
    }

    /**
     * Show the form for editing the specified BukuTabungan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var BukuTabungan $bukuTabungan */
        $bukuTabungan = BukuTabungan::find($id);

        if (empty($bukuTabungan)) {
            Flash::error('Buku Tabungan not found');

            return redirect(route('bukuTabungans.index'));
        }
        $jenisTransaksis = $this->bukuTabungan->jenis_transaksis;

        return view('buku_tabungans.edit',compact(('jenisTransaksis')))->with('bukuTabungan', $bukuTabungan);
    }

    /**
     * Update the specified BukuTabungan in storage.
     *
     * @param int $id
     * @param UpdateBukuTabunganRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBukuTabunganRequest $request)
    {
        /** @var BukuTabungan $bukuTabungan */
        $bukuTabungan = BukuTabungan::find($id);

        if (empty($bukuTabungan)) {
            Flash::error('Buku Tabungan not found');

            return redirect(route('bukuTabungans.index'));
        }

        $bukuTabungan->fill($request->all());
        $bukuTabungan->save();

        Flash::success('Buku Tabungan updated successfully.');

        return redirect(route('bukuTabungans.index'));
    }

    /**
     * Remove the specified BukuTabungan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BukuTabungan $bukuTabungan */
        $bukuTabungan = BukuTabungan::find($id);

        if (empty($bukuTabungan)) {
            Flash::error('Buku Tabungan not found');

            return redirect(route('bukuTabungans.index'));
        }

        $bukuTabungan->delete();

        Flash::success('Buku Tabungan deleted successfully.');

        return redirect(route('bukuTabungans.index'));
    }
}
