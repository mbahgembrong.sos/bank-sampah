<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSampahRequest;
use App\Http\Requests\UpdateSampahRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\JenisSampah;
use App\Models\Sampah;
use Illuminate\Http\Request;
use Flash;
use Response;

class SampahController extends AppBaseController
{
    public function __construct() {
        $this->sampah = new Sampah();
    }
    /**
     * Display a listing of the Sampah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Sampah $sampahs */
        $sampahs = Sampah::all();

        return view('sampahs.index')
            ->with('sampahs', $sampahs);
    }

    /**
     * Show the form for creating a new Sampah.
     *
     * @return Response
     */
    public function create()
    {
        $jenisSampahs = JenisSampah::pluck('nama', 'id');
        $satuans = $this->sampah->satuans;
        return view('sampahs.create', compact('jenisSampahs', 'satuans'));
    }

    /**
     * Store a newly created Sampah in storage.
     *
     * @param CreateSampahRequest $request
     *
     * @return Response
     */
    public function store(CreateSampahRequest $request)
    {
        $input = $request->all();

        /** @var Sampah $sampah */
        $sampah = Sampah::create($input);

        Flash::success('Sampah saved successfully.');

        return redirect(route('sampahs.index'));
    }

    /**
     * Display the specified Sampah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Sampah $sampah */
        $sampah = Sampah::find($id);

        if (empty($sampah)) {
            Flash::error('Sampah not found');

            return redirect(route('sampahs.index'));
        }

        return view('sampahs.show')->with('sampah', $sampah);
    }

    /**
     * Show the form for editing the specified Sampah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Sampah $sampah */
        $sampah = Sampah::find($id);

        if (empty($sampah)) {
            Flash::error('Sampah not found');

            return redirect(route('sampahs.index'));
        }
        $jenisSampahs = JenisSampah::pluck('nama', 'id');
        $satuans = $this->sampah->satuans;

        return view('sampahs.edit', compact('jenisSampahs','satuans'))->with('sampah', $sampah);
    }

    /**
     * Update the specified Sampah in storage.
     *
     * @param int $id
     * @param UpdateSampahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSampahRequest $request)
    {
        /** @var Sampah $sampah */
        $sampah = Sampah::find($id);

        if (empty($sampah)) {
            Flash::error('Sampah not found');

            return redirect(route('sampahs.index'));
        }

        $sampah->fill($request->all());
        $sampah->save();

        Flash::success('Sampah updated successfully.');

        return redirect(route('sampahs.index'));
    }

    /**
     * Remove the specified Sampah from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Sampah $sampah */
        $sampah = Sampah::find($id);

        if (empty($sampah)) {
            Flash::error('Sampah not found');

            return redirect(route('sampahs.index'));
        }

        $sampah->delete();

        Flash::success('Sampah deleted successfully.');

        return redirect(route('sampahs.index'));
    }
}
