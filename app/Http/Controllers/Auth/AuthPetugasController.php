<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Petugas;
use Illuminate\Http\Request;

class AuthPetugasController extends Controller
{
    protected $maxAttempts = 3;
    protected $decayMinutes = 2;
    protected $redirectTo = 'login.petugas';


    public function __construct()
    {
        $this->middleware('guest:petugas')->except('logout');
    }
    public function login()
    {
        return view('auth.petugas.login');
    }
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (auth()->guard('petugas')->attempt($request->only('email', 'password'))) {
            $petugas = Petugas::where('email', $request->email)->first();
            $request->session()->regenerate();
            // $this->clearLoginAttempts($request);
            return redirect()->route('home.petugas');
        } else {
            $this->incrementLoginAttempts($request);
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(["Incorrect user login details!"]);
        }
    }
    public function logout(Request $request)
    {
        auth()->guard('petugas')->logout();
        session()->flush();
        auth()->guard('petugas')->logout();

        return redirect()->route('petugas.login');
    }
}
