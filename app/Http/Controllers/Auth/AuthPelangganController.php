<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Nasabah;
use Illuminate\Http\Request;

class AuthPelangganController extends Controller
{
    protected $maxAttempts = 3;
    protected $decayMinutes = 2;
    protected $redirectTo = 'login.nasabah';


    public function __construct()
    {
        $this->middleware('guest:nasabah')->except('logout');
    }
    public function login()
    {
        return view('auth.pelanggan.login');
    }
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (auth()->guard('nasabah')->attempt($request->only('email', 'password'))) {
            $nasabah = Nasabah::where('email', $request->email)->first();
            $request->session()->regenerate();
            // $this->clearLoginAttempts($request);
            return redirect()->route('home.nasabah');
        } else {
            $this->incrementLoginAttempts($request);
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(["Incorrect user login details!"]);
        }
    }
    public function register()
    {
        return view('auth.pelanggan.register');
    }
    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required|email|unique:nasabah',
            'password' => 'required|confirmed'
        ]);
        $nasabah = Nasabah::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
    }
    public function logout(Request $request)
    {
        auth()->guard('nasabah')->logout();
        session()->flush();
        auth()->guard('nasabah')->logout();

        return redirect()->route('nasabah.login');
    }
}
