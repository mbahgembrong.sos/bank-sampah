<?php

namespace App\Http\Controllers;

use App\Models\BukuTabungan;
use CreateBukuTabungansTable;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class SetoranController extends Controller
{
    public function __construct()
    {
        $this->bukuTabungan = new BukuTabungan();
    }
    /**
     * Display a listing of the BukuTabungan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var BukuTabungan $bukuTabungans */
        $bukuTabungans = BukuTabungan::all();

        return view('setorans.index')
            ->with('bukuTabungans', $bukuTabungans);
    }

    /**
     * Show the form for creating a new BukuTabungan.
     *
     * @return Response
     */
    public function create()
    {
        $jenisTransaksis = $this->bukuTabungan->jenis_transaksis;
        return view('setorans.create', compact('jenisTransaksis'));
    }

    /**
     * Store a newly created BukuTabungan in storage.
     *
     * @param CreateBukuTabunganRequest $request
     *
     * @return Response
     */
    public function store(CreateBukuTabungansTable $request)
    {
        $input = $request->all();

        /** @var BukuTabungan $bukuTabungan */
        $bukuTabungan = BukuTabungan::create($input);

        Flash::success('Buku Tabungan saved successfully.');

        return redirect(route('setorans.index'));
    }
}
