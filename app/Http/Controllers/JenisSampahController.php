<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJenisSampahRequest;
use App\Http\Requests\UpdateJenisSampahRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\JenisSampah;
use Illuminate\Http\Request;
use Flash;
use Response;

class JenisSampahController extends AppBaseController
{
    /**
     * Display a listing of the JenisSampah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var JenisSampah $jenisSampahs */
        $jenisSampahs = JenisSampah::all();

        return view('jenis_sampahs.index')
            ->with('jenisSampahs', $jenisSampahs);
    }

    /**
     * Show the form for creating a new JenisSampah.
     *
     * @return Response
     */
    public function create()
    {
        return view('jenis_sampahs.create');
    }

    /**
     * Store a newly created JenisSampah in storage.
     *
     * @param CreateJenisSampahRequest $request
     *
     * @return Response
     */
    public function store(CreateJenisSampahRequest $request)
    {
        $input = $request->all();

        /** @var JenisSampah $jenisSampah */
        $jenisSampah = JenisSampah::create($input);

        Flash::success('Jenis Sampah saved successfully.');

        return redirect(route('jenisSampahs.index'));
    }

    /**
     * Display the specified JenisSampah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var JenisSampah $jenisSampah */
        $jenisSampah = JenisSampah::find($id);

        if (empty($jenisSampah)) {
            Flash::error('Jenis Sampah not found');

            return redirect(route('jenisSampahs.index'));
        }

        return view('jenis_sampahs.show')->with('jenisSampah', $jenisSampah);
    }

    /**
     * Show the form for editing the specified JenisSampah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var JenisSampah $jenisSampah */
        $jenisSampah = JenisSampah::find($id);

        if (empty($jenisSampah)) {
            Flash::error('Jenis Sampah not found');

            return redirect(route('jenisSampahs.index'));
        }

        return view('jenis_sampahs.edit')->with('jenisSampah', $jenisSampah);
    }

    /**
     * Update the specified JenisSampah in storage.
     *
     * @param int $id
     * @param UpdateJenisSampahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJenisSampahRequest $request)
    {
        /** @var JenisSampah $jenisSampah */
        $jenisSampah = JenisSampah::find($id);

        if (empty($jenisSampah)) {
            Flash::error('Jenis Sampah not found');

            return redirect(route('jenisSampahs.index'));
        }

        $jenisSampah->fill($request->all());
        $jenisSampah->save();

        Flash::success('Jenis Sampah updated successfully.');

        return redirect(route('jenisSampahs.index'));
    }

    /**
     * Remove the specified JenisSampah from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var JenisSampah $jenisSampah */
        $jenisSampah = JenisSampah::find($id);

        if (empty($jenisSampah)) {
            Flash::error('Jenis Sampah not found');

            return redirect(route('jenisSampahs.index'));
        }

        $jenisSampah->delete();

        Flash::success('Jenis Sampah deleted successfully.');

        return redirect(route('jenisSampahs.index'));
    }
}
