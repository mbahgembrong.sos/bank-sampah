<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePetugasRequest;
use App\Http\Requests\UpdatePetugasRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Petugas;
use Illuminate\Http\Request;
use Flash;
use Response;

class PetugasController extends AppBaseController
{
    public function __construct()
    {
        $this->petugas = new Petugas();
    }
    /**
     * Display a listing of the Petugas.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Petugas $petugas */
        $petugas = Petugas::all();
        return view('petugas.index')
            ->with('petugas', $petugas);
    }

    /**
     * Show the form for creating a new Petugas.
     *
     * @return Response
     */
    public function create()
    {
        $roles = $this->petugas->roles;
        return view('petugas.create', compact('roles'));
    }

    /**
     * Store a newly created Petugas in storage.
     *
     * @param CreatePetugasRequest $request
     *
     * @return Response
     */
    public function store(CreatePetugasRequest $request)
    {
        $input = $request->all();

        /** @var Petugas $petugas */
        $petugas = Petugas::create($input);

        Flash::success('Petugas saved successfully.');

        return redirect(route('petugas.index'));
    }

    /**
     * Display the specified Petugas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Petugas $petugas */
        $petugas = Petugas::find($id);

        if (empty($petugas)) {
            Flash::error('Petugas not found');

            return redirect(route('petugas.index'));
        }

        return view('petugas.show')->with('petugas', $petugas);
    }

    /**
     * Show the form for editing the specified Petugas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Petugas $petugas */
        $petugas = Petugas::find($id);
        if (empty($petugas)) {
            Flash::error('Petugas not found');

            return redirect(route('petugas.index'));
        }
        $roles = $this->petugas->roles;

        return view('petugas.edit', compact('roles'))->with('petugas', $petugas);
    }

    /**
     * Update the specified Petugas in storage.
     *
     * @param int $id
     * @param UpdatePetugasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePetugasRequest $request)
    {
        /** @var Petugas $petugas */
        $petugas = Petugas::find($id);

        if (empty($petugas)) {
            Flash::error('Petugas not found');

            return redirect(route('petugas.index'));
        }

        $petugas->fill($request->all());
        $petugas->save();

        Flash::success('Petugas updated successfully.');

        return redirect(route('petugas.index'));
    }

    /**
     * Remove the specified Petugas from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Petugas $petugas */
        $petugas = Petugas::find($id);

        if (empty($petugas)) {
            Flash::error('Petugas not found');

            return redirect(route('petugas.index'));
        }

        $petugas->delete();

        Flash::success('Petugas deleted successfully.');

        return redirect(route('petugas.index'));
    }
}
